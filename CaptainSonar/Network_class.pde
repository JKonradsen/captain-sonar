import processing.net.*;
import hypermedia.net.*;
UDP udp;



class NetworkHandler{
  String localIP;
  Server server;
  Client client;
  long UDPTimer = 0;
  long UDPTime = 10000;
  
  NetworkHandler(Server s){
     this.server = s;
     
     udp = new UDP( this, 12344 );
     println(Server.ip());
  }
  
  void loop(){
    
    client = server.available();
    if (client != null) {
      
      String input = client.readString();
      println(input);
      
      
      //It has something to with engineer 1
      if(input.contains("F1")){
        println("Found something with boat 1");
        
        if(input.startsWith("A")){
          println("Updated last connection time");
           boat1.updateLastConnectionTime(millis()); 
        }
        
        client.write(" F1:H=" + boat1.health);
        client.write(" F1:ML=" + (boat1.weapons.mineLevel+1));
        client.write(" F1:DL=" + boat1.weapons.droneLevel);
        client.write(" F1:SL=" + boat1.weapons.sonarLevel);
        client.write(" F1:TL=" + boat1.weapons.torpedoLevel);
        client.write(" F1:SIL=" + boat1.weapons.silenceLevel);
        //print("Sent to boat" + " F1:H=" + boat1.health);
        
        
      }
    }else{
       //print("."); 
    }
    
    if(!server.active()){
       print("Server encountered problems"); 
    }
    
    if(UDPTimer + UDPTime < millis()){
      println("Sending UDP");
      UDPTimer = millis();
      
      udp.send( "Server IP Broadcast", "192.168.1.255", 12344 );
      
    }
    
    
  }
  
}
