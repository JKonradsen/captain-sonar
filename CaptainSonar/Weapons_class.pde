/*
Class to hold all information about the teams weapons charge and so on.
*/

//Weapons definitions
final static int MINE    = 0;
final static int DRONE   = 1;
final static int SILENCE = 2;
final static int TORPEDO = 3;
final static int SONAR   = 4;


class Weapons{
  
  
  
  int mineLevel = 0;
  final int mineLevelMax = 2;
  
  int droneLevel = 0;
  final int droneLevelMax = 3;
  
  int silenceLevel = 0;
  final int silenceLevelMax = 5;
  
  int torpedoLevel = 0;
  final int torpedoLevelMax = 2;
  
  int sonarLevel = 0;
  final int sonarLevelMax = 2;
  
  Weapons(){
    
  }
  
  void increaseWeaponsLevel(int type){
    
    switch(type){
     case MINE:
       mineLevel++;
       break;
     case DRONE:
       droneLevel++;
       break;
     case SILENCE:
       silenceLevel++;
       break;
     case TORPEDO:
       torpedoLevel++;
       break;
     case SONAR:
       sonarLevel++;
       break;
    }
  }
  
  boolean checkWeaponFire(int type){
    
    switch(type){
     case MINE:
       if(mineLevel >= mineLevelMax){
         return true;  
       }
       break;
     case DRONE:
       if(droneLevel >= droneLevelMax){
         return true;  
       }
       break;
     case SILENCE:
       if(silenceLevel >= silenceLevelMax){
         return true;  
       }
       break;
     case TORPEDO:
       if(torpedoLevel >= torpedoLevelMax){
         return true;  
       }
       break;
     case SONAR:
       if(sonarLevel >= sonarLevelMax){
         return true;  
       }
       break;
    }
    return false;
  }
  
  void clearWeaponLevel(int type){ 
    
    switch(type){
     case MINE:
       mineLevel = 0;
       break;
     case DRONE:
       droneLevel = 0;
       break;
     case SILENCE:
       silenceLevel = 0;
       break;
     case TORPEDO:
       torpedoLevel = 0;
       break;
     case SONAR:
       sonarLevel = 0;
       break;
    }
  }
  
}
