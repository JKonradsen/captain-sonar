//Boat class definition initialization
class Boat{
  int xPos = 0;
  int yPos = 0;
  int health = 3;
  color c;
  int id;
  int SURFACE_TIME = 10;
  long surfaceTimer = 0;
  boolean surfaced = false;
  ArrayList<Path> path = new ArrayList<Path>();
  ArrayList<Mine> mines = new ArrayList<Mine>();
  Weapons weapons = new Weapons();
  long lastConnectTime = 0;
  final long acceptableAliveTime = 10000;
  
  
  Boat(color newColor, int idTemp){
    c = newColor;
    id = idTemp;
  }
  
  int getXpos(){
   return xPos; 
  }
  
  int getYpos(){
   return yPos; 
  }
  
  void drawStatus(int x, int y){
    fill(255);
    //stroke(255);
    
    PFont f;
    f = createFont("Arial", 14);
    textFont(f);
    textAlign(LEFT);
    text("Boat "+ id, x,y);
    text("Health "+ health, x,y+20);
    text("Mine "+ weapons.mineLevel + "/" + weapons.mineLevelMax, x,y+40);
    text("Torpedo "+ weapons.torpedoLevel + "/" + weapons.torpedoLevelMax, x,y+60);
    text("Drone "+ weapons.droneLevel + "/" + weapons.droneLevelMax, x,y+80);
    text("Silence "+ weapons.silenceLevel + "/" + weapons.silenceLevelMax, x,y+100);
    text("Sonar "+ weapons.sonarLevel + "/" + weapons.sonarLevelMax, x,y+120);
    if(this.lastConnectTime + acceptableAliveTime <  millis()){
        text("Connection status FAIL", x, y+140);
    }else{
        text("Connection status OK", x, y+140);
    }
    
    
  }
  
  void drawBoat(){
     fill(c); 
     if(surfaced){
       tint(100, 255);
     }else{
       tint(255, 126);
     }
     image(boat, xPos * xSpace + (xSpace/4), yPos * ySpace + (ySpace/4), xSpace*0.7, ySpace/2);
     textAlign(CENTER, CENTER);
     text("B" + id, xPos * xSpace + (xSpace/2), yPos * ySpace + (ySpace/2));
     
     if(surfaced){
       println("Time: " + millis()/1000 + "/" + (surfaceTimer + SURFACE_TIME));
       if(millis()/1000 > surfaceTimer + SURFACE_TIME){
         surfaced = false;
         clearPath();
         addPathStep(xPos, yPos);
       }
     }
  }
  
  void drawBoatPath(){
    for(Path step : path){
      fill(c);
      if(id == 1){
         rect(step.x*xSpace,step.y*ySpace,xSpace,ySpace/5); 
      }else{
         rect(step.x*xSpace,step.y*ySpace + (ySpace - ySpace/5),xSpace, ySpace/5);
      }
    }
  }
  
  
  void addPathStep(int x, int y){
    path.add(new Path( x, y));
  }
  
  void clearPath(){
    path.clear(); 
  }
  
  void moveBoat(int newX, int newY){
    addPathStep(newX, newY);
    xPos = newX;
    yPos = newY;
  }
  
  boolean checkBoatPath(int x, int y){
   
   //Check to see if boat is surfaced
   if(surfaced){
       return false;
   } 
    
   for(Path step : path){
     
     if(step.x == x && step.y == y){
       return false;
     }
   }
   return true;
  }
  
  void plantMine(int id, int x, int y){
    mines.add(new Mine(id, x, y));
  }
  
  void drawMines(){
    for(Mine mine : mines){
       fill(c);
       if(id==1){
         textAlign(RIGHT, CENTER);
       }else{
         textAlign(LEFT, CENTER);
       }
       text("M" + mine.mineID, mine.xPos * xSpace + (xSpace/2), mine.yPos * ySpace + (ySpace/2));
    }
  }
  
  int checkForWeaponDamage(int x, int y){
    float distFromBlast = sqrt( pow((float)(x - xPos),2) + pow((float)(y - yPos),2));
    println("Dist: " +distFromBlast);
    if( distFromBlast < 1.0){
      return 2;
    }else if(distFromBlast < 2.0){
      return 1;
    }
    
    return 0;
  }
  
  void mineFired(int x, int y){
    int damage = checkForWeaponDamage(x, y);
    health -= damage;
    println("Boat took " + damage + " and is now at " + health + " health");
  }
  
  void torpedoFired(int x, int y){
    int damage = checkForWeaponDamage(x, y);
    health -= damage;
    println("Boat took " + damage + " and is now at " + health + " health");
    
  }
  
  void surfaceBoat(){
    surfaced = true;
    surfaceTimer = millis()/1000;
  }
    
  
  void updateLastConnectionTime(long time){
     this.lastConnectTime = time;
  }
    
    
  
  
  
  
}
