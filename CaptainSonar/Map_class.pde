/*
* Class to hold the loaded map
*/

class Map{
  int xSize;
  int ySize;
  int tileMap[][];
  
  
  
  
  Map(){
    
    
  }
  
  
  void drawMapOutline(){
    fill(0,0,255);
    rect(0,0,width-menuX, height);
    
    //Make the lines the 
    fill(0);
    for(int x = 0; x < xSize; x++){
      line((x+1)*xSpace, 0, (x+1)*xSpace, height);
    }
    for(int y = 0; y < ySize; y++){
      line(0, (y+1)*ySpace, width-menuX,(y+1)*ySpace); 
    }
    
    for(int y = 0; y < ySize; y++){
      for(int x = 0; x <  xSize; x++){
        
        if(tileMap[x][y] == 1){
          fill(0);
          rect(x*xSpace, y*ySpace, xSpace, ySpace);
          tint(255, 200);
          image(land, x*xSpace, y*ySpace, xSpace, ySpace);
        }else{
          fill(120);
          rect(x*xSpace, y*ySpace, xSpace, ySpace);
          tint(255, 200);
          image(sea, x*xSpace, y*ySpace, xSpace, ySpace);
          
        }
        
        
        
      }
    }
    
  }

  void loadMapFromFile(){
    //Get the correct map file
    String fileName = "Map " + mapSelected + ".txt";
    String[] lines = loadStrings(fileName);
    
    //Update map size
    xSize = lines[0].length()/2 + 1;
    ySize = lines.length;
    
    //Update global spaces
    xSpace = (width - menuX) / xSize;
    ySpace = height / ySize;
    
    //Update tilemap
    tileMap = new int[xSize][ySize];
    
    for(int y = 0; y < ySize; y++){
     for(int x = 0; x <  xSize; x++){
        if( lines[y].charAt(x*2) == 'X'){
          tileMap[x][y] = 0;
        }else if(lines[y].charAt(x*2) == '0'){
          tileMap[x][y] = 1;
        }else{
          tileMap[x][y] = 2;
        }
     }
    }
  }
  
  boolean checkMapPosition(int x, int y){
     if(x < 0 || x > xSize-1){
      return false; 
     }
     if(y < 0 || y > ySize-1){
      return false; 
     }
     
     if(tileMap[x][y] == 1){
       return false;
     }
     
     return true;
    
  }
  
  
}
