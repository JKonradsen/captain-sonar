/**
* Author: Jakob Konradsen
* Date: 

*/

//Imports

//Menu definitions
int menuX = 200;

//Global variables
boolean gameStarted = false;
Boat boat1 = new Boat(color(100,100,0), 1);
Boat boat2 = new Boat(color(100,0,100), 2);
int xSpace;
int ySpace;

Map map = new Map();
int mapSelected = 0;

PImage boat, land, sea;
Server s;
NetworkHandler networkHandle = null;

void setup(){
  size(900, 600);
  background(0,0,255);
  frameRate(15); //Could consider to use noLoop() in order to only handle redrawing on events
  
  //Start network stuff
  s = new Server(this, 12345);
  networkHandle = new NetworkHandler(s);
  
  
  //Load images which must for some reason be done here
  boat = loadImage("submarine.png");
  land = loadImage("land.jpg");
  sea = loadImage("sea.jpg");
  
  //Init main menu to the right
  initMenu();
  
  println("Program setup finished");
  
  map.loadMapFromFile();
  map.drawMapOutline();
  
  
  
}

void draw(){
  boat1.drawBoat();
  boat2.drawBoat();
  
  boat1.drawBoatPath();
  boat2.drawBoatPath();
  
  boat1.drawMines();
  boat2.drawMines();
  
  initMenu();
  boat1.drawStatus(width - (menuX) + 10, 140);
  boat2.drawStatus(width - (menuX) + 10, 300);
  
  networkHandle.loop();
}




void initMenu(){
  
 stroke(0);
 fill(0);
 rect(width - menuX, 0, width - menuX, height);
 textAlign(LEFT);
 PFont f;
 f = createFont("Arial", 16);
 textFont(f);
 fill(255);
 text("Menu", width - (menuX/2), 20);
 text("Press 0 - 9 to choose map", width - (menuX/2)-90, 50);
 text("Press space to start game", width - (menuX/2)-90, 80);
 
}

void keyPressed() {
  
  //If it was 0 - 9, meaning it was map select
  if(key > '0' && key < '9'){
    println("Map selected: " + key);
    //Choose map
  }
  
  //If start of game is wanted
  if(key == 'l'){
    println("Start of game is wanted"); 
    gameStarted = true;
  }
  
  //Press d for debug stop
  if(key == 'p'){
    print("Stopped for debugging"); //<>//
  }
  
  //Press d for debug stop
  if(key == 't'){
    boat1.surfaceBoat();
  }
  
  //Will clear when using weapon
  if(key == 'c'){
    boat1.clearPath();
  }
  
  
  
  //Press keys for for debug stop
  if(keyCode == RIGHT){
    println("Moved boat to the right");
    if(map.checkMapPosition(boat1.xPos+1, boat1.yPos)&& boat1.checkBoatPath(boat1.xPos+1, boat1.yPos)){
      boat1.moveBoat(boat1.xPos+1, boat1.yPos);
    }
  }
  if(keyCode == LEFT){
    println("Moved boat to the left");
    if(map.checkMapPosition(boat1.xPos-1, boat1.yPos)&& boat1.checkBoatPath(boat1.xPos-1, boat1.yPos)){
      boat1.moveBoat(boat1.xPos-1, boat1.yPos);
    }
     
  }
  if(keyCode == UP){
    println("Moved boat up");
    if(map.checkMapPosition(boat1.xPos, boat1.yPos-1) && boat1.checkBoatPath(boat1.xPos, boat1.yPos-1)){
      boat1.moveBoat(boat1.xPos, boat1.yPos-1);
    }
    
  }
  if(keyCode == DOWN){
    println("Moved boat down");
    if(map.checkMapPosition(boat1.xPos, boat1.yPos+1) && boat1.checkBoatPath(boat1.xPos, boat1.yPos+1)){
    boat1.moveBoat(boat1.xPos, boat1.yPos+1);
    }
    
  }
  
  if(key == 'd'){
    println("Moved boat to the right");
    if(map.checkMapPosition(boat2.xPos+1, boat2.yPos)&& boat2.checkBoatPath(boat2.xPos+1, boat2.yPos)){
      boat2.moveBoat(boat2.xPos+1, boat2.yPos);
    }
  }
  if(key == 'a'){
    println("Moved boat to the left");
    if(map.checkMapPosition(boat2.xPos-1, boat2.yPos)&& boat2.checkBoatPath(boat2.xPos-1, boat2.yPos)){
      boat2.moveBoat(boat2.xPos-1, boat2.yPos);
    }
     
  }
  if(key == 'w'){
    println("Moved boat up");
    if(map.checkMapPosition(boat2.xPos, boat2.yPos-1) && boat2.checkBoatPath(boat2.xPos, boat2.yPos-1)){
      boat2.moveBoat(boat2.xPos, boat2.yPos-1);
    }
    
  }
  if(key == 's'){
    println("Moved boat down");
    if(map.checkMapPosition(boat2.xPos, boat2.yPos+1) && boat2.checkBoatPath(boat2.xPos, boat2.yPos+1)){
    boat2.moveBoat(boat2.xPos, boat2.yPos+1);
    }
    
  }
  
  if(key == 'm'){
    boat1.plantMine(1, boat1.xPos, boat1.yPos);
  }
  
  if(key == 'p'){
    boat1.mineFired(0, 0);
    boat2.mineFired(0, 0);
    boat1.weapons.checkWeaponFire(MINE);
  }
  map.drawMapOutline();
  
}

void mousePressed(){
  
  //Place team 1 boat
  if(mouseButton == LEFT){
      
  }
  
  //Place team 2 boat
  if(mouseButton == RIGHT){
    
  }
  
}
