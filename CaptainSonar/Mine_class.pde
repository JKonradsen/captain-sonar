/*
* Class for mines
* 
*/

class Mine{
  int xPos;
  int yPos;
  int mineID;
  
  Mine(int id, int x, int y){
    
    xPos = x;
    yPos = y;
    mineID = id;
  }
  
}
