
long aliveMessageTimer = 0;



int extractIdentifierValue(String input, String identifier){

  String subLine = input.substring(input.indexOf(identifier)+identifier.length()+1, input.length());
  //Serial.println("-"+subLine+"-");
  return subLine.toInt();

}


void handleServerCommunication(){
long now = millis();


if( aliveMessageTimer + ALIVE_MESSAGE_DELAY < now){
  checkServerConnection();
  aliveMessageTimer = now;
  sendAliveMessage();
}

while(client.available()){
  String line = client.readStringUntil('\r');

  //Make sure that the message was ment for this role
  if(line.indexOf(ROLE) > 0){

    Serial.println("Found my role!");
    
    if(line.indexOf("H") > 0){
      int health = extractIdentifierValue(line, "H");
      Serial.printf("Health is now: %i\n",health);
    }

    if(line.indexOf("DL") > 0){
      boatType.droneLevel = extractIdentifierValue(line, "DL");
      Serial.printf("DroneLevel is now: %i\n",boatType.droneLevel);
    }

    if(line.indexOf("SL") > 0){
      boatType.silenceLevel = extractIdentifierValue(line, "SL");
      Serial.printf("SonarLevel is now: %i\n",boatType.sonarLevel);
    }

    if(line.indexOf("TL") > 0){
      boatType.torpedoLevel = extractIdentifierValue(line, "TL");
      Serial.printf("TorpedoLevel is now: %i\n",boatType.torpedoLevel);
    }
    
    if(line.indexOf("SIL") > 0){
      boatType.silenceLevel = extractIdentifierValue(line, "SIL");
      Serial.printf("SilenceLevel is now: %i\n",boatType.silenceLevel);
    }

    if(line.indexOf("ML") > 0){
      boatType.mineLevel = extractIdentifierValue(line, "ML");
      Serial.printf("MineLevel is now: %i\n",boatType.mineLevel);
    }

  }
  Serial.println(line);
}

}

void initServerCommunication(){

  if (!client.connect(SERVER_IP.c_str(), SERVER_PORT)) {
    Serial.println("connection failed");
  }else{
    Serial.print("Connection made");
  }
}

boolean checkServerConnection(){
    if(!client.connected()){
      Serial.println("Lost connection, trying to re-establish connection");
      client.connect(SERVER_IP.c_str(), SERVER_PORT);
      return false;
  }else{
    return true;
  }
}

void sendAliveMessage(){
  String msg = (String)"A:" + (String)ROLE;
  client.print(msg);
}

void recieveServerIP(){
  int packetSize = Udp.parsePacket();
  if (packetSize)
  {
    Serial.printf("Recieved update on server IP =  %s, port %d\n",  Udp.remoteIP().toString().c_str(), Udp.remotePort());
    SERVER_IP = Udp.remoteIP().toString().c_str();
  }


}

