#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

//Wifi credentials
const char* ssid = "Fuglemorderen";
const char* wifi_password = "Flotfyr21";

//Function prototypes
void setup_wifi();

//Globals
WiFiClient client;  //Client to handle communication with the server
WiFiUDP Udp;  //UDP listener to listen for UDP with server IP

//Settings defines
String SERVER_IP = "0.0.0.0";
#define SERVER_PORT 12345
#define ALIVE_MESSAGE_DELAY 5000

//Specific role settings
String ROLE =  "F1";

//Boat type definition as typedef struct
typedef struct {
  //General attributes
  char health = 0;
  //Specific to the First Mate
  char mineLevel = 0;
  char droneLevel = 0;
  char silenceLevel = 0;
  char torpedoLevel = 0;
  char sonarLevel = 0;

  char mineLevelMax = 3;
  char droneLevelMax = 4;
  char sienceLevelMax = 6;
  char torpedoLevelMax = 3;
  char sonarLevelMax = 3;
}BoatClass;

BoatClass boatType;

void setup() {
  Serial.begin(115200);
  //Connect to local wifi
  setup_wifi();
  //Initiate communication with server
  initServerCommunication();

  Udp.begin(12344);
  
}

void loop() {

    handleServerCommunication();
    recieveServerIP();
}




